<?php defined('B_PROLOG_INCLUDED') and B_PROLOG_INCLUDED === true or die;

$askToChange = isset($arResult['ACTUAL_REGION']) && $arResult['REGION']['ID'] !== $arResult['ACTUAL_REGION']['ID'];

?>

<div id="region-select">
    <div>Город: <span data-entity="modal-open"><?= $arResult['LOCATION']->getName() ?></span></div>
    <div data-entity="confirmation" style="display: none;">
        <?php if($askToChange): ?>
            <div>
                Вы вошли на сайт региона <?=$arResult['REGION']['NAME']?>. <br>
                Хотите перейти на сайт Вашего региона <?=$arResult['ACTUAL_REGION']['NAME']?>?
            </div>
        <? else: ?>
            Ваш город <span><?= $arResult['LOCATION']->getName() ?></span>?
        <? endif ?>
        <button data-entity="confirm-city" data-city="<?=$arResult['LOCATION']->getId()?>">Да</button>
        <button data-entity="modal-open">Нет</button>
    </div>

    <div id="city-modal" data-entity="modal" style="display: none;">
        <div>
            <h4><b>Быстрый выбор</b></h4>
            <ul data-entity="favorites"></ul>
        </div>
        <div>
            <h4><b>Поиск города</b></h4>
            <input type="text" data-entity="search-input">
            <ul data-entity="cities"></ul>
        </div>

    </div>

</div>

<script type="text/javascript">
BX.ready(function() {
    new AppRegions({
        rootNode: 'region-select',
        askToChangeRegion: <?= var_export($askToChange)?>
    });
});
</script>
