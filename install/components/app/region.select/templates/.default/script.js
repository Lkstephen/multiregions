class AppRegions {
    constructor(params) {
        this.rootNode = BX(params.rootNode);
        this.openButtonNode = this.getEntities(this.rootNode, 'modal-open');
        this.favoriesNode = this.getEntity(this.rootNode, 'favorites');
        this.citiesNode = this.getEntity(this.rootNode, 'cities');
        this.searchInputNode = this.getEntity(this.rootNode, 'search-input');
        this.modal = this.getEntity(this.rootNode, 'modal');
        this.confirmationNode = this.getEntity(this.rootNode, 'confirmation');
        this.askToChangeRegion = params.askToChangeRegion;

        this.confirmCookie = 'app_regions_confirmed';

        this.cities = [];
        this.favorites = [];
        this.currentLocation = {};

        if (this.confirmationNode && (!this.isCityConfirmed() || this.askToChangeRegion)) {
            BX.style(this.confirmationNode, 'display', '');
            let confirm = this.getEntity(this.confirmationNode, 'confirm-city');
            BX.bind(confirm, 'click', (ev) => {
                let city = ev.target.dataset.city;
                city && this.confirmCity(city);
            });
        }

        this.openButtonNode.forEach((el) => {
            BX.bind(el, 'click', BX.proxy(this.openModal, this));
        });
    }

    getEntity(parent, entity, additionalFilter) {
        if (!parent || !entity)
            return null;

        additionalFilter = additionalFilter || '';

        return parent.querySelector(additionalFilter + '[data-entity="' + entity + '"]');
    }

    getEntities(parent, entity, additionalFilter) {
        if (!parent || !entity)
            return { length: 0 };

        additionalFilter = additionalFilter || '';

        return parent.querySelectorAll(additionalFilter + '[data-entity="' + entity + '"]');
    }

    isCityConfirmed() {
        return !!BX.getCookie(this.confirmCookie);
    }

    confirmCity(city) {
        this.setCity(city).then((data) => {
            BX.style(this.confirmationNode, 'display', 'none');
            if (data.redirect_url) {
                document.location = data.redirect_url;
            } else {
                document.location.reload();
            }
        });
    }

    openModal() {
        BX.style(this.confirmationNode, 'display', 'none');
        this.getInfo().then(() => {
            this.renderCityList();
            this.renderFavoriteList();
            this.rootNode.querySelectorAll('[data-city]').forEach(el => {
                BX.bind(el, 'click', (ev) => {
                        let city = ev.target.dataset.city;
                        city && this.confirmCity(city);
                    }
                );
            });
            BX.bind(this.searchInputNode, 'input', (e) => {
                const inputValue = e.target.value.toLowerCase().trim();
                const cities = this.citiesNode.querySelectorAll('li');
                cities.forEach((city) => {
                    let cityName = city.innerHTML.toLowerCase().trim();
                    if (!inputValue.length || cityName.substr(0, inputValue.length) !== inputValue) {
                        BX.style(city, 'display', 'none');
                    } else {
                        BX.style(city, 'display', '');
                    }
                });
            });
            $.fancybox.open(this.modal);
        }).catch(() => {
            alert('Ошибка при загрузке местоположений');
        });

    }

    setCity(id) {
        return new Promise((resolve, reject) => {
            if (!id) {
                return reject('Id is null');
            }
            BX.ajax.runComponentAction('app:region.select', 'setLocation', {
                data: {
                    locationId: id,
                    path: window.location.pathname + window.location.search
                }
            }).then(({ data }) => {
                resolve(data);
            }).catch((error) => {
                reject(error);
            });
        });
    }

    renderCityList() {
        BX.cleanNode(this.citiesNode);
        BX.adjust(this.citiesNode, {
            children: this.cities.map((item) => {
                return BX.create('li', {
                    text: item.name,
                    style: {
                        display: 'none'
                    },
                    dataset: {
                        city: item.id
                    }
                });
            })
        });
    }

    renderFavoriteList() {
        BX.cleanNode(this.favoriesNode);
        BX.adjust(this.favoriesNode, {
            children: this.favorites.map((item) => {
                return BX.create('li', {
                    text: item.name,
                    dataset: {
                        city: item.id
                    }
                });
            })
        });
    }

    getInfo() {
        return new Promise(((resolve, reject) => {
            if (this.cities.length) {
                return resolve();
            }

            BX.ajax.runComponentAction('app:region.select', 'getInfo').then(({ data }) => {
                this.cities = data.cities || [];
                this.favorites = data.favoriteList || [];
                this.currentLocation = data.currentLocation || {};
                resolve(data);
            }).catch((error) => {
                reject(error);
            });
        }));
    }
}
