<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

$arComponentDescription = [
    "NAME" => "Выбор региона",
    "ICON" => "",
    "SORT" => 500,
    "PATH" => [
        "ID" => "app",
        'NAME' => 'Awteam (ИП Лукьянченко С.С.)',
        "CHILD" => [
            "ID" => "regions",
            "NAME" => "Мультирегиональность",
            "SORT" => 500,
        ],
    ],
];
?>
