<?php

use Bitrix\Main\Context;
use Bitrix\Main\Engine\Controller;
use Bitrix\Main\Loader;
use Bitrix\Main\Web\Cookie;
use App\Regions\RegionManager;

class AppRegionSelectController extends Controller
{
    public function configureActions(): array
    {
        return [
            'getInfo' => [
                'prefilters' => [],
            ],
            'setLocation' => [
                'prefilters' => [],
            ],
        ];
    }

    public function getInfoAction(): array
    {
        $manager = RegionManager::getInstance();
        $location = $manager->getCurrentLocation();
        $actualRegion = $manager->getRegionByLocationId($location->getId());

        return [
            'currentLocation' => $location,
            'actualRegion' => [
                'ID' => $actualRegion['ID'],
                'NAME' => $actualRegion['NAME'],
            ],
            'favoriteList' => $manager->getLocationRepository()->getFavorites(),
            'cities' => $manager->getLocationRepository()->getCities(),
        ];
    }

    public function setLocationAction($locationId, $path = '')
    {
        $manager = RegionManager::getInstance();
        $region = $manager->getRegionByLocationId($locationId);
        if ($region) {
            $response = Context::getCurrent()->getResponse();

            $expires = strtotime('+1 year');
            $regionIdCookie = (new Cookie('app_regions_id', $region['ID'], $expires, false))->setHttpOnly(false);
            $response->addCookie($regionIdCookie);

            $locationIdCookie = (new Cookie('app_regions_location_id', $locationId, $expires, false))->setHttpOnly(false);
            $response->addCookie($locationIdCookie);

            $confirmationCookie = (new Cookie('app_regions_confirmed', 1, $expires, false))->setHttpOnly(false);
            $response->addCookie($confirmationCookie);
        }
        if ($redirect = $manager->getRedirectUrl($region, $path)) {
            return [
                'redirect_url' => $redirect,
            ];
        }

        return [
            'region' => $region,
        ];
    }

    protected function init()
    {
        parent::init();
        Loader::requireModule('app.regions');
    }
}
