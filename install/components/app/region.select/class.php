<?php

use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use App\Regions\RegionManager;

class AppRegionSelectComponent extends CBitrixComponent
{
    private $moduleDependencies;

    /**
     * @var RegionManager
     */
    private $regionManager;

    /**
     * AppRegionSelectComponent constructor.
     *
     * @param CBitrixComponent|null $component
     */
    public function __construct(?CBitrixComponent $component = null)
    {
        parent::__construct($component);
        $this->moduleDependencies = [
            'app.regions'
        ];
    }

    public function onPrepareComponentParams($arParams)
    {
        $this->checkDependencies();
        $this->regionManager = RegionManager::getInstance();

        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * @throws LoaderException
     */
    public function checkDependencies(): void
    {
        foreach ($this->moduleDependencies as $module) {
            Loader::requireModule($module);
        }
    }

    public function executeComponent(): void
    {
        $region = $this->regionManager->getCurrentRegion();
        if (!$region) {
            if (user()->IsAdmin()) {
                echo "Для работы компонента app:region.select необходимо создать хотя бы один регион";
            }
            return;
        }
        global $APPLICATION;
        if ($redirect = $this->regionManager->getRedirectUrl($region, $APPLICATION->GetCurPageParam())) {
            LocalRedirect($redirect, true, '301 Moved Permanently');
        }

        $this->prepareResult();
        $this->includeComponentTemplate();
    }

    private function prepareResult(): void
    {
        if ($location = $this->regionManager->getCurrentLocation()) {
            $this->arResult = [
                'LOCATION' => $location,
                'REGION' => $this->regionManager->getCurrentRegion(),
                'ACTUAL_REGION' => $this->regionManager->getRegionByLocationId($location->getId())
            ];
        }

    }
}
