CREATE TABLE IF NOT EXISTS `app_regions`
(
    `ID`            int(10) UNSIGNED        NOT NULL AUTO_INCREMENT,
    `IS_ACTIVE`     tinyint(4)              NOT NULL DEFAULT '1',
    `SORT`          int(11)                 NOT NULL DEFAULT '100',
    `NAME`          varchar(255)            NOT NULL,
    `NAME_PREP`     varchar(255)                     DEFAULT NULL,
    `CODE`          varchar(255)            NOT NULL,
    `SUB_DOMAIN`    varchar(255)            NOT NULL,
    `SITE_ID`       char(2)                 NOT NULL,
    `GROUP_ID`      int(10) UNSIGNED        NOT NULL,
    `MANAGER_ID`    int(10) UNSIGNED        NOT NULL,
    `PRICE_MOD`     varchar(12)                      DEFAULT NULL,
    `PRICE_VALUE`   decimal(10, 2) UNSIGNED NOT NULL DEFAULT 0,
    `LOCATION_CODE` varchar(100)            NOT NULL,
    `META_HEADER`   text,
    `META_BODY`     text,
    PRIMARY KEY (`ID`),
    UNIQUE KEY `SUB_DOMAIN_SITE_ID` (`SUB_DOMAIN`, `SITE_ID`),
    KEY `SUB_DOMAIN` (`SUB_DOMAIN`),
    KEY `SITE_ID` (`SITE_ID`),
    KEY `GROUP_ID` (`GROUP_ID`)
);


CREATE TABLE IF NOT EXISTS `app_region_price`
(
    `REGION_ID` int(10) unsigned NOT NULL,
    `PRICE_ID`  int(10) unsigned NOT NULL,
    PRIMARY KEY (`REGION_ID`, `PRICE_ID`)
);


CREATE TABLE IF NOT EXISTS `app_region_store`
(
    `REGION_ID` int(10) unsigned NOT NULL,
    `STORE_ID`  int(10) unsigned NOT NULL,
    PRIMARY KEY (`REGION_ID`, `STORE_ID`)
);

CREATE TABLE IF NOT EXISTS `app_region_location`
(
    `REGION_ID`     int(10) unsigned NOT NULL,
    `LOCATION_CODE` varchar(100)     NOT NULL,
    `LOCATION_TYPE` char(2)          NOT NULL DEFAULT 'L',
    PRIMARY KEY (`REGION_ID`, `LOCATION_CODE`, `LOCATION_TYPE`)
);
