<?php

use Bitrix\Main\Application;
use Bitrix\Main\EventManager;
use Bitrix\Main\IO\File;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\SystemException;

class app_regions extends CModule
{
    const MODULE_ID = 'app.regions';

    const MODULE_VERSION = '0.0.1';

    const MODULE_VERSION_DATE = '2019-06-17 00:00:00';

    const MODULE_NAME = 'Мультирегиональность';

    const MODULE_DESCRIPTION = 'Добавляет возможность разделения функционала сайта по регионам';

    const PARTNER_NAME = 'Awteam (ИП Лукьянченко С.С.)';

    const PARTNER_URI = 'https://awteam.ru';

    /**
     * @var EventManager
     */
    private $eventManager;

    function __construct()
    {
        $this->MODULE_ID = self::MODULE_ID;
        $this->MODULE_VERSION = self::MODULE_VERSION;
        $this->MODULE_VERSION_DATE = self::MODULE_VERSION_DATE;
        $this->MODULE_NAME = self::MODULE_NAME;
        $this->MODULE_DESCRIPTION = self::MODULE_DESCRIPTION;
        $this->PARTNER_NAME = self::PARTNER_NAME;
        $this->PARTNER_URI = self::PARTNER_URI;
        $this->eventManager = EventManager::getInstance();
    }

    public function doInstall()
    {
        global $APPLICATION;

        $this->InstallFiles();
        $this->installDB();
        $this->InstallEvents();

        ModuleManager::registerModule($this->MODULE_ID);

        $APPLICATION->IncludeAdminFile("Установка модуля", $this->getPath() . "/install/step.php");

        return true;
    }

    /**
     * @return bool|void
     * @throws SystemException
     */
    public function doUninstall()
    {
        global $APPLICATION;

        if (self::IncludeModule($this->MODULE_ID)) {
            $context = Application::getInstance()->getContext();
            $request = $context->getRequest();

            switch (intval($request->get('step'))) {
                case 0:
                    {
                        $APPLICATION->IncludeAdminFile("Удаление модуля", $this->getPath() . "/install/unstep1.php");

                        return true;
                    }
                case 1:
                    {
                        $this->UnInstallFiles();
                        $this->UnInstallEvents();

                        if ($request->get('savedata') !== 'Y') {
                            $this->UnInstallDB();
                        }

                        ModuleManager::unRegisterModule($this->MODULE_ID);

                        $APPLICATION->IncludeAdminFile("Удаление модуля", $this->getPath() . "/install/unstep2.php");

                        return true;
                    }
                default:
                    {
                        break;
                    }
            }
        }

        return false;
    }

    public function installDB()
    {
        global $DB;

        $errors = $DB->RunSQLBatch(__DIR__ . "/db/install.sql");

        if ($errors) {
            ShowError(implode(', ', $errors));

            return false;
        }

        return true;
    }

    public function UnInstallDB()
    {
        global $DB;

        $errors = $DB->RunSQLBatch(__DIR__ . "/db/uninstall.sql");

        if ($errors) {
            ShowError(implode(', ', $errors));

            return false;
        }

        return true;
    }

    public function InstallFiles()
    {
        CopyDirFiles(__DIR__ . "/components", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/components", true, true);
        CopyDirFiles(__DIR__ . "/admin", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin", true, true);
        CopyDirFiles(__DIR__ . "/../themes", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/themes", true, true);

        $path =  new \Bitrix\Main\IO\Directory($this->getPath() . '/admin');
        if ($path->isExists()) {
            $files = $path->getChildren();
            foreach ($files as $file) {
                if ($file->isFile() && str_replace('_', '.', substr($file->getName(), 0, strlen($this->MODULE_ID))) === $this->MODULE_ID) {
                    $innerPath = end(explode($_SERVER['DOCUMENT_ROOT'], $file->getPath()));
                    file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/' . $file->getName(),
                        '<?php require_once($_SERVER[\'DOCUMENT_ROOT\'] . \'' . $innerPath . '\'); ?>');
                }
            }
        }
    }

    public function UnInstallFiles()
    {
        DeleteDirFiles(__DIR__ . "/components", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/components");
        DeleteDirFiles(__DIR__ . "/admin", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin");
        DeleteDirFiles(__DIR__ . "/../themes", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/themes");

        $path =  new \Bitrix\Main\IO\Directory($this->getPath() . '/admin');
        if ($path->isExists()) {
            $files = $path->getChildren();
            foreach ($files as $file) {
                $copy = $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin/" . $file->getName();
                if (File::isFileExists($copy)) {
                    File::deleteFile($copy);
                }
            }
        }
    }

    public function InstallEvents()
    {
        foreach (self::getEvents() as $event) {
            $this->eventManager->registerEventHandler($event[0], $event[1], $this->MODULE_ID, $event[2], $event[3]);
        }
    }

    public function UnInstallEvents()
    {
        foreach (self::getEvents() as $event) {
            $this->eventManager->unRegisterEventHandler($event[0], $event[1], $this->MODULE_ID, $event[2], $event[3]);
        };
    }

    private function getPath($notDocumentRoot = false)
    {
        return ($notDocumentRoot)
            ? preg_replace('#^(.*)\/(local|bitrix)\/modules#', '/$2/modules', dirname(__DIR__))
            : dirname(__DIR__);
    }

    private function getEvents(): array
    {
        return [
            [
                'main',
                'OnBuildGlobalMenu',
                'App\Regions\EventHandler',
                'createMenu',
            ],
            [
                'main',
                'OnEndBufferContent',
                'App\Regions\EventHandler',
                'OnEndBufferContent',
            ],
            [
                'iblock',
                'OnIBlockPropertyBuildList',
                'App\Regions\EventHandler',
                'getRegionProperty',
            ]
        ];
    }
}
