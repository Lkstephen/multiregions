<?php

if (!check_bitrix_sessid()) {
    return false;
}

if ($e = $APPLICATION->GetException()) {
    echo (new CAdminMessage([
        "TYPE" => "ERROR",
        "MESSAGE" => "Ошибка при удалении модуля",
        "DETAILS" => $e->GetString(),
        "HTML" => true,
    ]))->Show();
} else {
    echo (new CAdminMessage(['MESSAGE' => 'Модуль удалён', 'TYPE' => 'OK']))->Show();
}
?>

<form action="<?= $APPLICATION->GetCurPage() ?>">
    <input type="hidden" name="lang" value="<?= LANGUAGE_ID ?>">
    <input type="submit" name="" value="Назад">
</form>
