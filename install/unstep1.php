<?php
use \Bitrix\Main\Localization\Loc;

if (!check_bitrix_sessid()) {
    return false;
}

Loc::loadMessages(__FILE__);

?>

<form action="<?=$APPLICATION->GetCurPage()?>">
    <?=bitrix_sessid_post()?>
    <input type="hidden" name="lang" value="<?=LANGUAGE_ID?>">
    <input type="hidden" name="id" value="<?=AppRegions::MODULE_ID?>">
    <input type="hidden" name="uninstall" value="Y">
    <input type="hidden" name="step" value="1">

    <?=(new CAdminMessage("Удаление модуля"))->Show()?>

    <p>Удаление модуля</p>
    <p><input type="checkbox" value="Y" name="savedata" id="savedata" value="Y" checked>
        <label for="savedata">Сохранить данные</label>
    </p>

    <input type="submit" name="" value="Удалить">

</form>
