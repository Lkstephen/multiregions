<?php
/**
 * @global CUser $USER
 * @global CMain $APPLICATION
 */

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use App\Regions\Tables\RegionTable;

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

$APPLICATION->SetTitle("Регионы");

if (!$USER->CanDoOperation('edit_other_settings') && !$USER->CanDoOperation('view_other_settings')) {
    $APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));
}

$isAdmin = $USER->CanDoOperation('edit_other_settings');

Loader::requireModule('app.regions');

$tableID = 'table_app.regions';
$sorting = new CAdminSorting($tableID, "ID", "DESC");
$adminList = new CAdminList($tableID, $sorting);
$adminList->InitFilter([
    'find_id',
    'find_name',
    'find_active',
]);
$filter = [];
/**
 * @global $find_id
 * @global $find_name
 * @global $find_code
 * @global $find_active
 */
if ($find_id) {
    $filter['=ID'] = $find_id;
}
if ($find_name) {
    $filter['%NAME'] = $find_name;
}
if ($find_active) {
    $filter['=IS_ACTIVE'] = $find_active;
}
if ($find_code) {
    $filter['%CODE'] = $find_code;
}

if ($adminList->EditAction() && $isAdmin) {
    foreach ($request["FIELDS"] as $ID => $arFields) {
        if (!$adminList->IsUpdated($ID)) {
            continue;
        }

        $result = RegionTable::update($ID, $arFields);
        if (!$result->isSuccess()) {
            $adminList->AddUpdateError("(ID=" . $ID . ") " . implode("<br>", $result->getErrorMessages()), $ID);
        }
    }
}

if (($arID = $adminList->GroupAction()) && $isAdmin) {
    if ($request['action_target'] == 'selected') {
        $arID = [];
        $data = RegionTable::getList(["filter" => $filter]);
        while ($region = $data->fetch()) {
            $arID[] = $region['ID'];
        }
        unset($region);
    }

    foreach ($arID as $ID) {
        if (intval($ID) <= 0) {
            continue;
        }

        switch ($request['action_button']) {
            case "delete":
                $result = RegionTable::delete($ID);
                if (!$result->isSuccess()) {
                    $adminList->AddGroupError("(ID=" . $ID . ") " . implode("<br>", $result->getErrorMessages()), $ID);
                }
                break;
        }
    }
}

$sortBy = strtoupper($sorting->getField());
if (!RegionTable::getEntity()->hasField($sortBy)) {
    $sortBy = "ID";
}

$sortOrder = strtoupper($sorting->getOrder());
if ($sortOrder !== "ASC") {
    $sortOrder = "DESC";
}

$nav = new \Bitrix\Main\UI\AdminPageNavigation("nav_app.regions");
$regions = RegionTable::getList([
    'filter' => $filter,
    'order' => [$sortBy => $sortOrder],
    'count_total' => true,
    'offset' => $nav->getOffset(),
    'limit' => $nav->getLimit(),
]);
$nav->setRecordCount($regions->getCount());
$adminList->setNavigation($nav, "Страницы");

$entity = RegionTable::getEntity();
$fields = $entity->getFields();
$headers = [
    ["id" => "ID", "content" => $fields["ID"]->getTitle(), "sort" => "ID", "default" => true],
    ["id" => "NAME", "content" => $fields["NAME"]->getTitle(), "sort" => "NAME", "default" => false],
    ["id" => "CODE", "content" => $fields["CODE"]->getTitle(), "sort" => "CODE", "default" => true],
    ["id" => "SORT", "content" => $fields["SORT"]->getTitle(), "sort" => "SORT", "default" => true],
    ["id" => "SUB_DOMAIN", "content" => $fields["SUB_DOMAIN"]->getTitle(), "sort" => "SUB_DOMAIN", "default" => true],
    ["id" => "SITE", "content" => $fields["SITE"]->getTitle(), "sort" => "SITE", "default" => true],
    ["id" => "IS_ACTIVE", "content" => $fields["IS_ACTIVE"]->getTitle(), "sort" => "IS_ACTIVE", "default" => true],
    ["id" => "PRICES", "content" => $fields["PRICES"]->getTitle(), "sort" => "PRICES", "default" => true],
    ["id" => "STORES", "content" => $fields["STORES"]->getTitle(), "sort" => "STORES", "default" => true],
];
$USER_FIELD_MANAGER->AdminListAddHeaders(RegionTable::getUfId(), $headers);
$adminList->AddHeaders($headers);

while ($region = $regions->fetchObject()) {
    $id = $region->getId();

    $row = &$adminList->AddRow($id, $region->collectValues(), "app_regions_edit.php?ID=" . $id . "&lang=" . LANGUAGE_ID,
        "Изменить");

    $row->AddViewField("ID",
        '<a href="app_regions_edit.php?ID=' . $id . '&amp;lang=' . LANGUAGE_ID . '" title="Изменить">' . $id . '</a>');
    $row->AddInputField("NAME");
    $row->AddInputField("CODE");
    $row->AddInputField("SUB_DOMAIN");
    $row->AddCheckField("IS_ACTIVE");

    $region->fillSite();
    $row->AddViewField("SITE", $region->getSite()->getName());

    $region->fillPrices();
    $row->AddViewField("PRICES", implode(", ", $region->getPrices()->getNameList()));

    $region->fillStores();
    $row->AddViewField("STORES", implode(", ", $region->getStores()->getTitleList()));

    $arActions = [];
    $arActions[] = [
        "ICON" => "edit",
        "TEXT" => "Изменить",
        "ACTION" => $adminList->ActionRedirect("app_regions_edit.php?ID=" . $id),
    ];
    if ($isAdmin) {
        $arActions[] = [
            "ICON" => "copy",
            "TEXT" => "Копировать",
            "ACTION" => $adminList->ActionRedirect("app_regions_edit.php?COPY_ID=" . $id),
        ];
        $arActions[] = ["SEPARATOR" => true];
        $arActions[] = [
            "ICON" => "delete",
            "TEXT" => "Удалить",
            "ACTION" => "if(confirm('Удалить?')) " . $adminList->ActionDoGroup($id, "delete"),
        ];
    }

    $row->AddActions($arActions);
}

$adminList->AddGroupActionTable([
    "delete" => true,
]);

$aContext = [
    [
        "TEXT" => "Добавить",
        "LINK" => "app_regions_edit.php?lang=" . LANGUAGE_ID,
        "TITLE" => "Добавить",
        "ICON" => "btn_new",
    ],
];
$adminList->AddAdminContextMenu($aContext);

$adminList->CheckListMode();

require($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/include/prolog_admin_after.php");

?>
    <form name="find_form" method="GET" action="<?= $APPLICATION->GetCurPage() ?>?">
        <?

        $oFilter = new CAdminFilter(
            $tableID . "_filter",
            [
                "id" => $fields["ID"]->getTitle(),
                "name" => $fields["NAME"]->getTitle(),
                "active" => $fields["IS_ACTIVE"]->getTitle(),
            ]
        );

        $oFilter->Begin();
        ?>
        <tr>
            <td><?

                echo $fields["ID"]->getTitle() ?>:
            </td>
            <td><input type="text" name="find_id" size="47" value="<?

                echo htmlspecialcharsbx($find_id) ?>"></td>
        </tr>
        <tr>
            <td><?

                echo $fields["NAME"]->getTitle() ?>:
            </td>
            <td><input type="text" name="find_name" size="47" value="<?

                echo htmlspecialcharsbx($find_name) ?>"></td>
        </tr>
        <tr>
            <td><?

                echo $fields["CODE"]->getTitle() ?>:
            </td>
            <td><input type="text" name="find_code" size="47" value="<?

                echo htmlspecialcharsbx($find_code) ?>"></td>
        </tr>
        <tr>
            <td><?= $fields["IS_ACTIVE"]->getTitle() ?>:</td>
            <td><?

                $arr = [
                    "reference" => ["Да", "Нет"],
                    "reference_id" => [1, 0],
                ];
                echo SelectBoxFromArray("find_active", $arr, htmlspecialcharsbx($find_active), 'Все');
                ?></td>
        </tr>

        <?

        $oFilter->Buttons(["table_id" => $tableID, "url" => $APPLICATION->GetCurPage(), "form" => "find_form"]);
        $oFilter->End();
        ?>
    </form>
<?

$adminList->DisplayList();

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
