<?php

use Bitrix\Catalog\GroupTable;
use Bitrix\Catalog\StoreTable;
use Bitrix\Main\Context;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ORM\Data\AddResult;
use Bitrix\Main\ORM\EntityError;
use Bitrix\Main\ORM\Fields\BooleanField;
use Bitrix\Main\Text\HtmlFilter;
use App\Regions\Tables\RegionLocationTable;
use App\Regions\Tables\RegionTable;

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php"); ?>

<?php
$selfFolderUrl = $adminPage->getSelfFolderUrl();
$listUrl = $selfFolderUrl."app_regions_list.php?lang=".LANGUAGE_ID;
$listUrl = $adminSidePanelHelper->editUrlToPublicPage($listUrl);
$bVarsFromForm = false;
global $USER_FIELD_MANAGER, $USER;

if (!$USER->CanDoOperation('edit_other_settings') && !$USER->CanDoOperation('view_other_settings')) {
    $APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));
}

$isAdmin = $USER->CanDoOperation('edit_other_settings');

Loader::requireModule('app.regions');
Loader::requireModule('catalog');
Loader::requireModule('sale');

function fillObject($region)
{
    static $formFields = [
        'NAME',
        'NAME_PREP',
        'CODE',
        'SORT',
        "IS_ACTIVE",
        "SUB_DOMAIN",
        'SITE_ID',
        'GROUP_ID',
        'PRICE_MOD',
        'PRICE_VALUE',
        'LOCATION_CODE',
        'MANAGER_ID',
        'META_HEADER',
        'META_BODY'
    ];

    $request = Context::getCurrent()->getRequest();

    $fields = $region->entity->getFields();

    //set values from the form
    foreach ($formFields as $fieldName) {
        $value = $request->getPost($fieldName);
        if ($fields[$fieldName] instanceof BooleanField) {
            $value = $value === 'Y' || boolval($value);
        }

        if (empty($value)) {
            $value = null;
        }

        if ($fieldName === 'PRICE_VALUE') {
            $value = floatval(preg_replace('/[^\d.]/', '', $value));
        }

        if ($fieldName === 'SUB_DOMAIN') {
            // Заменяет невалидные символы
            $value = preg_replace('/(-)\1+/', '$1', preg_replace('/[^\w\d-]/', '', mb_strtolower($value)));
        }

        $region->set($fieldName, $value);
    }

    $prices = $request->getPost("PRICE");
    if ($region->hasId()) {
        $region->removeAllPrices();
    }
    if (is_array($prices)) {
        foreach ($prices as $id) {
            $price = GroupTable::getEntity()->wakeUpObject($id);
            $region->addToPrices($price);
        }
    }

    $stores = $request->getPost("STORE");
    if ($region->hasId()) {
        $region->removeAllStores();
    }
    if (is_array($stores)) {
        foreach ($stores as $id) {
            $store = StoreTable::getEntity()->wakeUpObject($id);
            $region->addToStores($store);
        }
    }
}
require_once($_SERVER['DOCUMENT_ROOT'] .'/bitrix/modules/main/include/prolog_admin_after.php');
$aTabs = [
    [
        "DIV" => "edit1",
        "TAB" => 'Регион',
        "ICON" => "message_edit",
        'TITLE' => 'Регион',
    ],
    [
        "DIV" => "edit_seo",
        "TAB" => 'SEO',
        "ICON" => "message_edit",
        'TITLE' => 'SEO',
    ],
    [
        "DIV" => "edit2",
        "TAB" => 'Цены',
        "ICON" => "message_edit",
        'TITLE' => 'Цены',
    ],
    [
        "DIV" => "edit3",
        "TAB" => 'Склады',
        "ICON" => "message_edit",
        'TITLE' => 'Склады',
    ],
    [
        "DIV" => "edit4",
        "TAB" => 'Местоположения',
        "ICON" => "message_edit",
        'TITLE' => 'Местоположения',
    ],
];
if((count($USER_FIELD_MANAGER->GetUserFields('APP_REGIONS')) > 0) || ($USER_FIELD_MANAGER->GetRights('APP_REGIONS') >= "W")) {
    $aTabs[] = $USER_FIELD_MANAGER->EditFormTab('APP_REGIONS');
}

$tabControl = new CAdminForm("app_regions_edit", $aTabs);

$request = Context::getCurrent()->getRequest();
$ID = intval($request["ID"]);
$COPY_ID = intval($request["COPY_ID"]);

$entity = RegionTable::getEntity();
$fields = $entity->getFields();

$errors = [];
$region = null;

// Сохранение новой записи
if ($request->isPost() && ($request["save"] <> '' || $request["apply"] <> '') && $isAdmin && check_bitrix_sessid()) {
    if (empty($errors)) {
        if ($ID > 0 && !$COPY_ID) {
            $region = $entity->wakeUpObject($ID);
        } else {
            $region = $entity->createObject();
        }

        fillObject($region);
        $result = $region->save();

        if ($result instanceof AddResult) {
            $ID = $result->getId();
        }

        if ($ID > 0) {
            // Сохраняет местоположения
            $locationTypes = $request->getPost('LOCATION');
            $locations = [];
            foreach ($locationTypes as $type => &$value) {
                if ($value) {
                    $value = $locationCodes = explode(':', $value);
                    foreach ($locationCodes as $code) {
                        $locations[] = [
                            "LOCATION_CODE" => $code,
                            "LOCATION_TYPE" => $type
                        ];
                    }
                }
            }
            RegionLocationTable::resetMultipleForOwner($region->getId(), $locationTypes);

            // Сохранят пользовательские поля
            $data = [];
            global $USER_FIELD_MANAGER;
            $USER_FIELD_MANAGER->EditFormAddFields($entity->getUfId(), $data);
            if ($USER_FIELD_MANAGER->CheckFields($entity->getUfId(), $ID, $data))
            {
                $USER_FIELD_MANAGER->Update($entity->getUfId(), $ID, $data);
            } else {
                if (is_object($APPLICATION) && $APPLICATION->getException())
                {
                    $e = $APPLICATION->getException();
                    $result->addError(new EntityError($e->getString()));
                    $APPLICATION->resetException();
                }
                else
                {
                    $result->addError(new EntityError("Unknown error while checking userfields"));
                }
            }

        }

        if ($result->isSuccess()) {
            if ($request["save"] <> '') {
                LocalRedirect($listUrl);
            } else {
                LocalRedirect(sprintf('/bitrix/admin/app_regions_edit.php?lang=%s&ID=%s&%s', LANGUAGE_ID, $ID,
                    $tabControl->ActiveTabParam()));
            }
        } else {
            $errors = $result->getErrorMessages();
        }
    }
}

// Редактирование записи
if ($ID > 0 || $COPY_ID > 0) {
    $regionId = ($COPY_ID > 0 ? $COPY_ID : $ID);
    $uf = $USER_FIELD_MANAGER->GetUserFields(RegionTable::getUfId());
    $select = ['*'];
    foreach ($uf as $field) {
        $select[] = $field['FIELD_NAME'];
    }

    $region = RegionTable::query()
        ->setSelect($select)
        ->where('ID', $regionId)
        ->setLimit(1)
        ->fetchObject();
} else {
    $region = $entity->createObject();
}

if (!empty($errors)) {
    fillObject($region);
}

$APPLICATION->SetTitle(($ID > 0 ? 'Изменение региона' : 'Создание региона'));
?>

<?
$aMenu = [
    [
        "TEXT" => 'Все записи',
        "LINK" => $listUrl,
        "TITLE" => 'Все записи',
        "ICON" => "btn_list",
    ],
];

if ($ID > 0 && $isAdmin) {
    $aMenu[] = ["SEPARATOR" => "Y"];

    $aMenu[] = [
        "TEXT" => 'Создать',
        "LINK" => "app_regions_edit.php?lang=".LANGUAGE_ID,
        "TITLE" => 'Создать',
        "ICON" => "btn_new",
    ];
    $aMenu[] = [
        "TEXT" => 'Копировать',
        "LINK" => "app_regions_edit.php?lang=".LANGUAGE_ID."&amp;COPY_ID=".$ID,
        "TITLE" => 'Копировать',
        "ICON" => "btn_copy",
    ];
    $aMenu[] = [
        "TEXT" => 'Удалить',
        "LINK" => "javascript:if(confirm('Удалить?')) window.location='app_regions_list.php?ID=".$ID."&lang=".LANGUAGE_ID."&".bitrix_sessid_get()
                  ."&action_button=delete';",
        "TITLE" => "Удалить",
        "ICON" => "btn_delete",
    ];
}

$context = new CAdminContextMenu($aMenu);
$context->Show();

if(!empty($errors))
{
    $bVarsFromForm = true;
    CAdminMessage::ShowMessage(join("\n", $errors));
}

?>
        <?
        $tabControl->BeginPrologContent();
        echo $USER_FIELD_MANAGER->ShowScript();
        $tabControl->EndPrologContent();

        $tabControl->BeginEpilogContent();
        ?>

        <?= GetFilterHiddens("filter_");?>
        <?=bitrix_sessid_post()?>
        <input type="hidden" name="lang" value="<?=LANGUAGE_ID?>">
        <input type="hidden" name="ID" value="<?= $ID?>">
        <?if($COPY_ID > 0):?><input type="hidden" name="COPY_ID" value="<?= $COPY_ID?>"><?endif?>

        <?
        $tabControl->EndEpilogContent();
        $tabControl->Begin([
            'FORM_ACTION' => $APPLICATION->GetCurPage().'?ID='.IntVal($ID).'&lang='
                .LANG,
        ]);

        $tabControl->BeginNextFormTab();
        ?>
        <?php $tabControl->BeginCustomField('id', ''); ?>
        <?if($ID > 0):?>
            <tr>
                <td width="40%"><?= $fields["ID"]->getTitle()?>:</td>
                <td width="60%"><?= $ID?></td>
            </tr>
        <?endif?>
        <?php $tabControl->EndCustomField('id'); ?>
        <?php $tabControl->BeginCustomField('is_active', ''); ?>
        <tr>
            <td width="40%"><label for="active"><?= $fields["IS_ACTIVE"]->getTitle()?>:</label></td>
            <td width="60%"><input type="checkbox" name="IS_ACTIVE" id="active" value="1"<?if($region->getIsActive()) echo " checked"?>></td>
        </tr>
        <?php $tabControl->EndCustomField('is_active'); ?>
        <?php $tabControl->BeginCustomField('name', ''); ?>
        <tr class="adm-detail-required-field">
            <td width="40%"><?= $fields["NAME"]->getTitle()?>:</td>
            <td width="60%">
                <input type="text" name="NAME" value="<?= $region->getName(); ?>">
            </td>
        </tr>
        <?php $tabControl->EndCustomField('name'); ?>
        <?php $tabControl->BeginCustomField('code', ''); ?>
        <tr class="adm-detail-required-field">
            <td width="40%"><?= $fields["CODE"]->getTitle()?>:</td>
            <td width="60%">
                <input type="text" name="CODE" value="<?= $region->getCode(); ?>">
            </td>
        </tr>
        <?php $tabControl->EndCustomField('code'); ?>
        <?php $tabControl->BeginCustomField('sort', ''); ?>
        <tr class="adm-detail-required-field">
            <td width="40%"><?= $fields["SORT"]->getTitle()?>:</td>
            <td width="60%">
                <input type="text" name="SORT" value="<?= $region->getSort(); ?>">
            </td>
        </tr>
        <?php $tabControl->EndCustomField('sort'); ?>
        <?php $tabControl->BeginCustomField('site_id', ''); ?>
        <tr class="adm-detail-required-field">
            <td width="40%"><?= $fields["SITE_ID"]->getTitle()?></td>
            <td width="60%"><?
                echo CLang::SelectBox("SITE_ID", $region->getSiteId());
                ?>
            </td>
        </tr>
        <?php $tabControl->EndCustomField('site_id'); ?>
        <?php $tabControl->BeginCustomField('group_id', ''); ?>
        <tr class="adm-detail-required-field">
            <td width="40%"><?= $fields["GROUP_ID"]->getTitle()?></td>
            <td width="60%"><?
                $result = \Bitrix\Main\GroupTable::query()
                    ->setSelect(['ID', 'NAME'])->fetchAll();
                $reference = [
                    'REFERENCE_ID' => [],
                    'REFERENCE' => []
                ];
                foreach ($result as $row) {
                    $reference['REFERENCE_ID'][$row['ID']] = $row['ID'];
                    $reference['REFERENCE'][$row['ID']] = $row['NAME'];
                }

                $currentGroup = ($region->getGroupId() > 0 ? $region->getGroupId() : 2); // ID 2 - это все пользователи
                echo SelectBoxFromArray(
                    'GROUP_ID',
                    $reference,
                    $currentGroup,
                    '',
                    'class="typeselect" style="min-width:320px;"'
                );
                ?>
            </td>
        </tr>
        <?php $tabControl->EndCustomField('group_id'); ?>
        <?php $tabControl->BeginCustomField('store', ''); ?>
        <tr class="adm-detail-required-field">
            <td width="40%">Город склада</td>
            <td width="60%"><?
                $result = CSaleLocation::GetList(["CITY_NAME_LANG" => "ASC"], [
                        'CITY_LID' => LANGUAGE_ID,
                        'COUNTRY_LID' => LANGUAGE_ID,
                        'REGION_LID' => LANGUAGE_ID,
                    ]);
                $reference = [
                        'REFERENCE_ID' => [],
                        'REFERENCE' => []
                ];
                while ($row = $result->Fetch()) {
                    $text = [
                        $row['CITY_NAME'],
                        $row['REGION_NAME'],
                        $row['COUNTRY_NAME']
                    ];
                    // Можно получить код и красивее, если будет время
                    $code = CSaleLocation::getLocationCODEbyID($row['ID']);
                    $reference['REFERENCE_ID'][$code] = $code;
                    $reference['REFERENCE'][$code] = sprintf('%s [%s]', implode(', ', $text), $row['ID']);
                }
                echo SelectBoxFromArray(
                        'LOCATION_CODE',
                    $reference,
                    $region->getLocationCode(),
                    '',
                    'class="typeselect" style="min-width:320px;"'
                    );
                ?>
            </td>
        </tr>
        <?php $tabControl->EndCustomField('store'); ?>
        <?php $tabControl->BeginCustomField('sub_domain', ''); ?>
        <tr class="adm-detail-required-field">
            <td width="40%"><?= $fields["SUB_DOMAIN"]->getTitle()?>:</td>
            <td width="60%"><input type="text" name="SUB_DOMAIN" size="30" maxlength="50" value="<?= HtmlFilter::encode($region->getSubDomain())?>" onfocus="window.bxCurrentControl=this"></td>
        </tr>
        <?php $tabControl->EndCustomField('sub_domain'); ?>
        <?php $tabControl->BeginCustomField('manager', ''); ?>
        <tr class="adm-detail-required-field">
            <td width="40%"><?= $fields["MANAGER_ID"]->getTitle()?>:</td>
            <td width="60%">
                <?php
                echo FindUserID(
                    "MANAGER_ID",
                    $region->getManagerId(),
                    "",
                    "app_regions_edit_form"
                );
                ?>
            </td>
        </tr>
        <?php $tabControl->EndCustomField('manager'); ?>
        <?

        $tabControl->BeginNextFormTab();
        // СЕО
        ?>
        <?php $tabControl->BeginCustomField('name_prep', ''); ?>
        <tr>
            <td width="40%"><?= $fields["NAME_PREP"]->getTitle()?>:</td>
            <td width="60%">
                <input type="text" name="NAME_PREP" value="<?= $region->getNamePrep(); ?>">
            </td>
        </tr>
        <?php $tabControl->EndCustomField('name_prep'); ?>
        <?php $tabControl->BeginCustomField('meta_header', ''); ?>
        <tr>
            <td width="40%"><?= $fields["META_HEADER"]->getTitle()?>:</td>
            <td width="60%">
                <?php
                CFileMan::AddHTMLEditorFrame(
                    "META_HEADER",
                    $region["META_HEADER"],
                    "META_HEADER",
                    "text",
                    [
                        'height' => 200,
                        'width'  => '100%',
                    ],
                    "N",
                    0,
                    "",
                    "",
                    false,
                    true,
                    false,
                    ['hideTypeSelector' => true]
                );
                ?>
            </td>
        </tr>
        <?php $tabControl->EndCustomField('meta_header'); ?>
        <?php $tabControl->BeginCustomField('meta_body', ''); ?>
        <tr>
            <td width="40%"><?= $fields["META_BODY"]->getTitle()?>:</td>
            <td width="60%">
                <?php
                CFileMan::AddHTMLEditorFrame(
                    "META_BODY",
                    $region["META_BODY"],
                    "META_BODY",
                    "text",
                    [
                        'height' => 200,
                        'width'  => '100%',
                    ],
                    "N",
                    0,
                    "",
                    "",
                    false,
                    true,
                    false,
                    ['hideTypeSelector' => true]
                );
                ?>
            </td>
        </tr>
        <? $tabControl->EndCustomField('meta_body');

        $tabControl->BeginNextFormTab();
        // Цены
        ?>

        <?php $tabControl->BeginCustomField('prices', ''); ?>
        <tr>
            <td width="40%"><?=$fields['PRICES']->getTitle()?>:</td>
            <td width="60%">
                <?php
                $prices = GroupTable::query()
                    ->setSelect(['ID', 'NAME', 'LANG_NAME' => 'CURRENT_LANG.NAME'])
                    ->fetchAll();
                $reference = [];
                foreach ($prices as $price) {
                    $reference['REFERENCE_ID'][$price['ID']] = $price['ID'];
                    $reference['REFERENCE'][$price['ID']] = sprintf('[%s] %s', $price['NAME'], $price['LANG_NAME']);
                }
                $currentPrices = [];
                if ($region->hasId()) {
                    $region->fillPrices();
                    $currentPrices = $region->getPrices()->getIdList();
                }
                echo SelectBoxMFromArray(
                    "PRICE[]",
                    $reference,
                    $currentPrices,
                    "",
                    false,
                    10,
                    'class="typeselect" style="min-width:320px;"'
                );
                ?>
            </td>
        </tr>
        <?php $tabControl->EndCustomField('prices'); ?>
        <?php $tabControl->BeginCustomField('price_mod', ''); ?>
        <tr>
            <td><?=$fields['PRICE_MOD']->getTitle()?>:</td>
            <td>
                <?php
                echo SelectBoxFromArray(
                    'PRICE_MOD',
                    [
                        'REFERENCE_ID' => [
                            RegionTable::FIX_UP => RegionTable::FIX_UP,
                            RegionTable::FIX_DOWN => RegionTable::FIX_DOWN,
                            RegionTable::PERCENT_UP => RegionTable::PERCENT_UP,
                            RegionTable::PERCENT_DOWN => RegionTable::PERCENT_DOWN,
                        ],
                        'REFERENCE' => [
                            'Не изменять',
                            RegionTable::FIX_UP => 'Увеличить на фиксированное значение',
                            RegionTable::FIX_DOWN => 'Уменьшить на фиксированное значение',
                            RegionTable::PERCENT_UP => 'Увеличить на количество процентов',
                            RegionTable::PERCENT_DOWN => 'Уменьшить на количество процентов',
                        ],
                    ],
                    $region->getPriceMod(),
                    '',
                    'class="typeselect" style="min-width:320px;"'
                );
                ?>
            </td>
        </tr>
        <?php $tabControl->EndCustomField('price_mod'); ?>
        <?php $tabControl->BeginCustomField('price_value', ''); ?>
        <tr>
            <td><?=$fields['PRICE_VALUE']->getTitle()?>:</td>
            <td><input type="text" name="PRICE_VALUE" value="<?=$region->getPriceValue()?>"></td>
        </tr>
        <?php $tabControl->EndCustomField('price_value'); ?>

        <?php

        $tabControl->BeginNextFormTab();
        // Склады
        ?>
        <?php $tabControl->BeginCustomField('stores', ''); ?>
        <tr>
            <td width="40%"><?= $fields["STORES"]->getTitle()?>:</td>
            <td width="60%">
                <?
                $stores = StoreTable::query()
                    ->setFilter(['ISSUING_CENTER' => 'Y', 'ACTIVE' => 'Y'])
                    ->setSelect(['ID', 'TITLE'])
                    ->fetchAll();
                $reference = [];
                foreach ($stores as $store) {
                    $reference['REFERENCE_ID'][$store['ID']] = $store['ID'];
                    $reference['REFERENCE'][$store['ID']] = sprintf('[%s] %s', $store['ID'], $store['TITLE']);
                }
                ?>
                <?php

                $currentStores = [];
                if ($region->hasId()) {
                    $region->fillStores();
                    $currentStores = $region->getStores()->getIdList();
                }

                echo SelectBoxMFromArray(
                    "STORE[]",
                    $reference,
                    $currentStores,
                    "",
                    false,
                    10,
                    'style="min-width:200px"'
                );
                ?>
            </td>
        </tr>
        <?php $tabControl->EndCustomField('stores'); ?>
        <?php

        $tabControl->BeginNextFormTab();
        // Местоположения
        ?>
        <?php $tabControl->BeginCustomField('location', ''); ?>
        <tr class="adm-detail-required-field">
            <td>

                <?$APPLICATION->IncludeComponent("bitrix:sale.location.selector.system", "", array(
                    "ENTITY_PRIMARY" => $region->getId(),
                    "LINK_ENTITY_NAME" => RegionLocationTable::getEntity()->getFullName(),
                    "INPUT_NAME" => 'LOCATION',
                    "SELECTED_IN_REQUEST" => array(
                        'L' => isset($_REQUEST['LOCATION']['L']) ? explode(':', $_REQUEST['LOCATION']['L']) : false,
                        'G' => isset($_REQUEST['LOCATION']['G']) ? explode(':', $_REQUEST['LOCATION']['G']) : false
                    )
                ),
                    false
                );?>

            </td>
        </tr>
        <?php $tabControl->EndCustomField('location'); ?>
        <?
        $tabControl->BeginNextFormTab();
//        var_dump($region->collectValues());
        echo $tabControl->ShowUserFieldsWithReadyData(
            RegionTable::getUfId(),
            $region->collectValues(),
            $bVarsFromForm,
            'ID'
        );

        $tabControl->Buttons(array("disabled"=>!$isAdmin, "back_url"=>$listUrl));
        $tabControl->Show();
        ?>


<?php require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php"); ?>
