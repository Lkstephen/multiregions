<?
/** @global CMain $APPLICATION */

use Bitrix\Main\Config\Option;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use App\Regions\Helpers;
use App\Regions\Tables\RegionTable;

$module_id = app_regions::MODULE_ID;
Loader::requireModule($module_id);

$moduleAccessLevel = $APPLICATION->GetGroupRight($module_id);

if ($moduleAccessLevel >= 'R') {
    $pageUrl = $APPLICATION->GetCurPage().'?lang='.LANGUAGE_ID.'&mid='.$module_id ;
    $aTabs = [
        ["DIV" => "edit0", "TAB" => "Настройки"],
        ["DIV" => "edit1", "TAB" => "Права"],
    ];
    $tabControl = new CAdminTabControl("TabControl", $aTabs, true, true);

    if ($_SERVER['REQUEST_METHOD'] == "GET"
        && !empty($_GET['RestoreDefaults'])
        && $moduleAccessLevel == "W"
        && check_bitrix_sessid()) {

        $v1 = "id";
        $v2 = "asc";
        $z = CGroup::GetList($v1, $v2, ["ACTIVE" => "Y", "ADMIN" => "N"]);
        while ($zr = $z->Fetch()) {
            $APPLICATION->DelGroupRight($module_id, [$zr["ID"]]);
        }

        LocalRedirect($pageUrl);
    }

    if ($_SERVER['REQUEST_METHOD'] == 'POST'
        && $moduleAccessLevel == "W"
        && check_bitrix_sessid()) {
        if (isset($_POST['Update']) && $_POST['Update'] === 'Y') {

            if (isset($_POST['region'])) {
                Option::set(AppRegions::MODULE_ID, 'default_region', $_POST['region']);
            }

            Option::set(AppRegions::MODULE_ID, 'multi_domain', (isset($_POST['sub_domain']) ? 'Y' : 'N'));

            ob_start();
            require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/admin/group_rights.php');
            ob_end_clean();

            LocalRedirect($pageUrl.'&'.$tabControl->ActiveTabParam());
        }
    }

    $tabControl->Begin();
    ?>
    <form method="POST" action="<?= $pageUrl ?>" name="regions_settings">
        <? echo bitrix_sessid_post();

        $tabControl->BeginNextTab();
        ?>
        <tr>
            <td width="40%">Использовать поддомены</td>
            <td width="60%">
                <input type="checkbox" name="sub_domain" <?=Helpers::isMultiDomainEnabled() ? 'checked' : '' ?>>
            </td>
        </tr>
        <tr>
            <td width="40%">Основной регион сайта</td>
            <td width="60%"><select name="region"><?

                    $regions = RegionTable::query()
                        ->where('IS_ACTIVE', true)
                        ->setOrder(['ID', 'SORT'])
                        ->setSelect(['ID', 'NAME'])
                        ->fetchAll();

                    $selectedRegion = Helpers::getDefaultRegionId();

                    foreach ($regions as $region) {
                        ?>
                        <option value="<?= $region['ID']; ?>"<?= ((int)$region['ID'] === $selectedRegion ? ' selected' : ''); ?>>
                        <?= htmlspecialcharsex($region['NAME']); ?>
                        </option><?
                    }
                    unset($region, $regions);

                    ?></select></td>
        </tr>

        <?

        $tabControl->BeginNextTab();

        require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/admin/group_rights.php");

        $tabControl->Buttons(); ?>
        <script type="text/javascript">
            function RestoreDefaults() {
                if (confirm('<? echo CUtil::JSEscape("Вы уверены?"); ?>'))
                    window.location = "<?= $pageUrl ?>&RestoreDefaults=Y&<?=bitrix_sessid_get()?>";
            }
        </script>
        <input type="submit"<?= ($moduleAccessLevel < 'W' ? ' disabled' : ''); ?> name="Update"
               value="<?= Loc::getMessage('MAIN_SAVE') ?>" class="adm-btn-save">
        <input type="hidden" name="Update" value="Y">
        <input type="reset" name="reset" value="<?= Loc::getMessage('MAIN_RESET') ?>">
        <input type="button"<?= ($moduleAccessLevel < 'W' ? ' disabled' : ''); ?> onclick="RestoreDefaults();"
               value="По-умолчанию">
    </form>
    <?

    $tabControl->End();
}
