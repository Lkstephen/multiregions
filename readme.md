# Мультирегиональность

Модуль позволяет организовать работу сайта в нескольких регионах.

## Возможности
- Работа на одном домене
- Свой поддомен для каждого региона
- Автоматическое определение местоположения пользователя
- Ручной выбор местоположения
- Привязка элементов инфоблока к региону
- Расширенное СЕО для регионов

## Требования
- PHP 7.1+
- Битрикс: Малый бизнес или выше

## Особенности установки и обновления модуля в битрикс
Модуль устанавливается в систему при помощи `composer` и плагина `composer installers`.
Для установки необходимо добавить в composer.json ссылку на репозиторий, поскольку пакет не размещен на packagist,
а так же настроить пути установки в composer installers

### Установка

Установка composer/installers
```
composer require composer/installers
```

Пример настройки пути установки. [Справка](https://github.com/composer/installers)
```
"extra": {
    "bitrix-dir": "public/local",
    "installer-paths": {
        "modules/{$vendor}.{$name}/": [
            "type:bitrix-d7-module"
        ]
    }
}
```

Добавление репозитория модуля
```
"repositories": [
    {
      "type": "vcs",
      "url": "git@gitlab.com:awteam/web/bitrix-modules/app.regions.git"
    }
]
```

Установка модуля
```
composer require app/regions
```

### Обновление
В угоду скорости разработки и отсутствия публичного доступа к модулю, процесс обновления кардинально отличается от стандартного процесса обновления модулей в битрикс.

При обновлении необходимо придерживаться следующей последовательности:
1. Произвести удаление модуля с сохранением данных в панели битрикс, без стирания.
2. Выполнить миграции таблиц БД, если имеются.
3. Выполнить `composer install`.
4. Выполнить повторную установку модуля.

Процесс переустановки модуля является обязательным в тех случаях, когда обновляется код компонентов, изменяется процесс установки модуля (например, добавляются новые события, файлы и т.д)

## Добавление региона

... нужно написать

## robots.txt

Модуль позволяет преобразовывать оригинальный файл robots.txt в мультидоменный формат, подменяя название домана в Host и Sitemap
Для обработку файла отвечает класс `\App\Regions\Seo\Robots`

Разместить в корне сайта файл robots.php со следующим содержимым:
```php
<?php

use App\Regions\Seo\Robots;

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
define("NO_AGENT_CHECK", true);
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';
$_SESSION["SESS_SHOW_INCLUDE_TIME_EXEC"] = "N";
$APPLICATION->ShowIncludeStat = false;

header('Content-Type:text/plain');

$content = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/robots.txt');

if (\Bitrix\Main\Loader::requireModule('app.regions')) {
    $content = (new Robots($content))->getContent();
}

echo $content;

```

Далее необходимо должным образом настроить вебсервер, чтобы на обращения к robots.txt отвечал robots.php

в .htaccess это делается следующим образом
```apacheconfig
<IfModule mod_rewrite.c>
    Options +FollowSymLinks
    RewriteEngine On
    RewriteRule ^robots.txt$ robots.php [L,QSA]
</IfModule>
``` 

## Карта сайта 

Аналогично файлу robots.txt работает и карта сайта, подменяя ссылку в поле `<loc></loc>`

За обработку файла отвечает класс `\App\Regions\Seo\Sitemap`

Разместить в корне сайта файл sitemap.php со следующим содержимым

```php
<?php

use App\Regions\Seo\Sitemap;

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
define("NO_AGENT_CHECK", true);
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';
$_SESSION["SESS_SHOW_INCLUDE_TIME_EXEC"] = "N";
$APPLICATION->ShowIncludeStat = false;

header('Content-Type:text/xml');

$siteMapPath = $_SERVER['DOCUMENT_ROOT'] . '/sitemap.xml';

if (\Bitrix\Main\Loader::requireModule('app.regions')) {
    $siteMap = new Sitemap($_SERVER['HTTP_HOST']);
    if ($siteMapArray = $siteMap->parse($siteMapPath)) {
        echo $siteMap->xmlFromArray($siteMapArray);
        exit;
    }

}

echo file_get_contents($siteMapPath);

```

Далее необходимо должным образом настроить вебсервер, чтобы на обращения к sitemap.xml отвечал sitemap.php

в .htaccess это делается следующим образом
```apacheconfig
<IfModule mod_rewrite.c>
    Options +FollowSymLinks
    RewriteEngine On
    RewriteRule ^sitemap.xml$ sitemap.php [L,QSA]
</IfModule>
``` 
