<?php

namespace App\Regions;

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use CFile;
use App\Regions\Iblock\Properties\RegionPropertyType;
use App\Regions\Tables\RegionTable;

class EventHandler
{

    public static function createMenu(&$arGlobalMenu, &$arModuleMenu)
    {
        Menu::getAdminMenu($arGlobalMenu, $arModuleMenu);
    }

    /**
     * @param $content
     *
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\SystemException
     */
    public static function replaceRegionName(&$content)
    {
        $context = Application::getInstance()->getContext();
        $request = $context->getRequest();
        if (!$request->isAdminSection() && !$request->isAjaxRequest() && Loader::includeModule('app.regions')) {
            if ($region = RegionManager::getInstance()->getCurrentRegion()) {

                $fields = RegionTable::getDisplayFields();
                $replace = [];

                foreach ($fields as $code => $title) {
                    $replace['#REGION_' . $code . '#'] = $region[$code];
                }

                $content = str_replace(array_keys($replace), array_values($replace), $content);
            }
        }
    }

    public static function addSeoShortcuts(&$content)
    {
        global $APPLICATION;
        $page = $APPLICATION->GetCurPage();

        if (defined("ADMIN_SECTION") && (
                $page == '/bitrix/admin/cat_product_edit.php' ||
                $page == '/bitrix/admin/cat_section_edit.php' ||
                $page == '/bitrix/admin/iblock_element_edit.php' ||
                $page == '/bitrix/admin/iblock_section_edit.php')
            && Loader::includeModule('app.regions')) {
            $region = RegionManager::getInstance()->getCurrentRegion();
            if (!$region) {
                return;
            }

            $START_SEO_BLOCK = "BX.adminShowMenu(this, [{'TEXT'";
            $END_SEO_BLOCK = ", '');";
            $INPUT_WRAPPER = 'insertIntoInheritedPropertiesTemplate';
            $INPUT_DELIMITER = ", \'";
            $INPUT_WRAPPER_END = ")'";

            $lastPos = 0;
            $i = 0;
            while (($lastPos = strpos($content, $START_SEO_BLOCK, $lastPos)) !== false) {
                $end = strpos($content, $END_SEO_BLOCK, $lastPos);

                $startInputWrapper = strpos($content, $INPUT_WRAPPER, $lastPos);
                $input1Start = strpos($content, $INPUT_DELIMITER, $startInputWrapper);
                $input2Start = strpos($content, $INPUT_DELIMITER, $input1Start + 1);

                $input1 = substr(
                    $content,
                    $input1Start + strlen($INPUT_DELIMITER),
                    $input2Start - $input1Start - 2 - strlen($INPUT_DELIMITER)
                );

                $input2End = strpos($content, $INPUT_WRAPPER_END, $input2Start);

                $input2 = substr($content, $input2Start + 4, $input2End - $input2Start - 6);

                $str = ",\n{'TEXT':";
                $str .= "'Регион'";
                $str .= ",'MENU':[";
                $fields = RegionTable::getDisplayFields();
                foreach ($fields as $code => $name) {
                    $str .= "{'TEXT':'" . $name . "','ONCLICK':'InheritedPropertiesTemplates.insertIntoInheritedPropertiesTemplate(\'#REGION_" . $code . "#\', \'" . $input1 . "\', \'" . $input2 . "\')'},";
                }
                $str = substr($str, 0, strlen($str) - 1);
                $str .= ']}';

                $content = substr($content, 0, $end - 1) . $str . substr($content, $end - 1);

                $lastPos = $lastPos + strlen($START_SEO_BLOCK);
                if ($i > 30) {
                    break;
                }
                ++$i;
            }
        }
    }

    public static function getRegionProperty()
    {
        return RegionPropertyType::GetUserTypeDescription();
    }

    public static function OnEndBufferContent(&$content)
    {
        self::replaceRegionName($content);
        self::addSeoShortcuts($content);
    }
}
