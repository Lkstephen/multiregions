<?php

namespace App\Regions\Detectors;

/**
 * Этот интерфейс должен реализовать каждый класс, который отвечает за определение региона
 *
 * @package App\Regions\Detectors
 */
interface RegionDetector
{
    /**
     * @return int
     */
    public function getRegionId(): ?int;
}
