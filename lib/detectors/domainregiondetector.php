<?php

namespace App\Regions\Detectors;

use Bitrix\Main\ArgumentException;
use Bitrix\Main\ObjectPropertyException;
use Bitrix\Main\SystemException;
use App\Regions\Helpers;
use App\Regions\Tables\RegionTable;

/**
 * Осуществляет определение региона на основе доменного имени сайта, на который зашел пользователь
 *
 * @package App\Regions\Detectors
 */
final class DomainRegionDetector implements RegionDetector
{
    /**
     * @return int|null
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    public function getRegionId(): ?int
    {
        if ($domain = Helpers::getDomainInfo()) {
            $region = RegionTable::query()
                ->where('IS_ACTIVE', true)
                ->where('SITE_ID', SITE_ID)
                ->where('SUB_DOMAIN', $domain['SUB_DOMAIN'])
                ->addSelect('ID')
                ->fetch();
            if ($region) {
                return $region['ID'];
            }
        }

        return null;
    }
}
