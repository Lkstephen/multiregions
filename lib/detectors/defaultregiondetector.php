<?php

namespace App\Regions\Detectors;

use Bitrix\Main\ArgumentNullException;
use Bitrix\Main\ArgumentOutOfRangeException;
use App\Regions\Helpers;

/**
 * Определяет домен по-умолчанию для тех ситуаций, когда ни один регион не определен
 *
 * @package App\Regions\Detectors
 */
final class DefaultRegionDetector implements RegionDetector
{
    /**
     * @return int|null
     * @throws ArgumentNullException
     * @throws ArgumentOutOfRangeException
     */
    final public function getRegionId(): ?int
    {
        return Helpers::getDefaultRegionId();
    }
}
