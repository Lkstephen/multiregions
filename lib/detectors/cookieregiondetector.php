<?php

namespace App\Regions\Detectors;

use Bitrix\Main\Context;

/**
 * Определяет ранее установленный пользователем регион на основе данных Cookie
 *
 * @package App\Regions\Detectors
 */
final class CookieRegionDetector implements RegionDetector
{
    const REGION_COOKIE = 'app_regions_id';

    /**
     * @return int|null
     */
    final public function getRegionId(): ?int
    {
        if ($regionId = (int)Context::getCurrent()->getRequest()->getCookieRaw(self::REGION_COOKIE)) {
            return $regionId;
        }

        return null;
    }
}
