<?php

namespace App\Regions\Tables;

use Bitrix\Catalog\GroupTable;
use Bitrix\Catalog\StoreTable;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\ObjectPropertyException;
use Bitrix\Main\ORM\Data\DataManager;
use Bitrix\Main\ORM\Fields\BooleanField;
use Bitrix\Main\ORM\Fields\EnumField;
use Bitrix\Main\ORM\Fields\FloatField;
use Bitrix\Main\ORM\Fields\IntegerField;
use Bitrix\Main\ORM\Fields\Relations\ManyToMany;
use Bitrix\Main\ORM\Fields\Relations\Reference;
use Bitrix\Main\ORM\Fields\StringField;
use Bitrix\Main\ORM\Fields\TextField;
use Bitrix\Main\ORM\Query\Join;
use Bitrix\Main\SiteTable;
use Bitrix\Main\SystemException;
use Bitrix\Sale\Location\LocationTable;
use CUserTypeEntity;

final class RegionTable extends DataManager
{
    const PERCENT_UP = 'PERCENT_UP';

    const PERCENT_DOWN = 'PERCENT_DOWN';

    const FIX_UP = 'FIX_UP';

    const FIX_DOWN = 'FIX_DOWN';

    public static function getTableName(): string
    {
        return 'app_regions';
    }

    public static function getUfId(): string
    {
        return 'APP_REGIONS';
    }

    /**
     * @return array
     * @throws ArgumentException
     * @throws SystemException
     */
    public static function getMap(): array
    {
        return [
            (new IntegerField("ID"))
                ->configureTitle("ID")
                ->configurePrimary(true)
                ->configureAutocomplete(true),

            (new BooleanField("IS_ACTIVE"))
                ->configureTitle('Активность')
                ->configureStorageValues(0, 1)
                ->configureDefaultValue(1),

            (new IntegerField('SORT'))
                ->configureTitle('Сортировка'),

            (new StringField('NAME'))
                ->configureTitle('Название')
                ->configureRequired(true),

            (new StringField('NAME_PREP'))
                ->configureTitle('Город в предложном падеже')
                ->configureRequired(true),

            (new StringField('CODE'))
                ->configureTitle('Код')
                ->configureRequired(true),

            (new StringField('SUB_DOMAIN'))
                ->configureTitle('Поддомен')
                ->configureRequired(true),

            (new StringField('SITE_ID'))
                ->configureTitle('Сайт')
                ->configureRequired(true),

            (new IntegerField('GROUP_ID'))
                ->configureTitle('Группа для посетителей')
                ->configureRequired(true),

            (new IntegerField('MANAGER_ID'))
                ->configureTitle('Ответственный менеджер')
                ->configureRequired(true),

            (new TextField('META_HEADER'))
                ->configureTitle('Код в header'),

            (new TextField('META_BODY'))
                ->configureTitle('Код в body'),

            (new StringField('LOCATION_CODE'))
                ->configureTitle('Основной город')
                ->configureRequired(true),

            (new EnumField('PRICE_MOD'))
                ->configureTitle('Модификатор цены')
                ->configureValues([self::FIX_UP, self::FIX_DOWN, self::PERCENT_UP, self::PERCENT_DOWN]),

            (new FloatField('PRICE_VALUE'))
                ->configureTitle('Значение модификатора')
                ->configureScale(2)
                ->configureDefaultValue(0),

            (new Reference('SITE', SiteTable::class, Join::on('this.SITE_ID', 'ref.LID')))
                ->configureTitle('Сайт'),

            (new Reference('LOCATION', LocationTable::class, Join::on('this.LOCATION_CODE', 'ref.CODE')))
                ->configureTitle('Город склада'),

            (new ManyToMany('PRICES', GroupTable::class))
                ->configureTitle('Цены')
                ->configureTableName('app_region_price')
                ->configureRemotePrimary('ID', 'PRICE_ID'),

            (new ManyToMany('STORES', StoreTable::class))
                ->configureTitle('Склады')
                ->configureTableName('app_region_store')
                ->configureRemotePrimary('ID', 'STORE_ID'),

            (new ManyToMany('LOCATIONS', LocationTable::class))
                ->configureTitle('Местоположения')
                ->configureMediatorEntity(RegionLocationTable::class),
        ];
    }

    /**
     * @param $id
     * @param  array  $parameters
     * @return array|null
     * @throws ArgumentException
     * @throws SystemException
     * @throws ObjectPropertyException
     */
    public static function getByLocationId(int $id, array $parameters = []): ?array
    {
        $parameters['filter']['=LOCATIONS.ID'] = $id;

        return self::getRow($parameters);
    }

    public static function getDisplayFields()
    {
        $codes = [
            'NAME',
            'CODE',
            'NAME_PREP',
        ];
        $userFieldsByName = [];
        foreach ($codes as $code) {
            $userFieldsByName[$code] = self::getEntity()->getField($code)->getTitle();
        }

        global $USER_FIELD_MANAGER;
        $userFields = $USER_FIELD_MANAGER->GetUserFields(RegionTable::getUfId());

        foreach ($userFields as $field) {
            $tmp = CUserTypeEntity::GetByID($field['ID']);
            $title = $tmp['FIELD_NAME'];
            if ($tmp['LIST_COLUMN_LABEL'][LANGUAGE_ID]) {
                $title = $tmp['LIST_COLUMN_LABEL'][LANGUAGE_ID];
            }
            $userFieldsByName[$tmp['FIELD_NAME']] = $title;
        }

        return $userFieldsByName;
    }
}
