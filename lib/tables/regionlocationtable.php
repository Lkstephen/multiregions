<?php

namespace App\Regions\Tables;

use Bitrix\Main\ArgumentException;
use Bitrix\Main\ORM\Fields\IntegerField;
use Bitrix\Main\ORM\Fields\Relations\Reference;
use Bitrix\Main\ORM\Fields\StringField;
use Bitrix\Main\ORM\Query\Join;
use Bitrix\Main\SystemException;
use Bitrix\Sale\Location\Connector;
use Bitrix\Sale\Location\GroupTable;
use Bitrix\Sale\Location\LocationTable;

/**
 * Промежуточная таблица для связи региона с местоположениями в битрикс
 *
 * @package App\Regions\Tables
 */
final class RegionLocationTable extends Connector
{

    public static function getTableName(): string
    {
        return 'app_region_location';
    }

    public static function getConnectType(): int
    {
        return self::LINK_CODE;
    }

    /**
     * @return array
     * @throws ArgumentException
     * @throws SystemException
     */
    public static function getMap(): array
    {
        return [
            (new IntegerField('REGION_ID'))
                ->configurePrimary(true)
                ->configureRequired(true),

            (new StringField('LOCATION_CODE'))
                ->configurePrimary(true)
                ->configureRequired(true),

            (new StringField('LOCATION_TYPE'))
                ->configurePrimary(true)
                ->configureRequired(true)
                ->configureDefaultValue(self::DB_LOCATION_FLAG),

            (new Reference('LOCATION', LocationTable::class, [
                '=this.LOCATION_CODE' => 'ref.CODE',
                '=this.LOCATION_TYPE' => ['?', static::DB_LOCATION_FLAG],
            ])),

            (new Reference('GROUP', GroupTable::class, [
                '=this.LOCATION_CODE' => 'ref.CODE',
                '=this.LOCATION_TYPE' => ['?', self::DB_GROUP_FLAG],
            ])),

            (new Reference('REGION', RegionTable::class, Join::on('this.REGION_ID', 'ref.ID'))),
        ];
    }

    public static function getLocationLinkField(): string
    {
        return 'LOCATION_CODE';
    }

    public function getLinkField(): string
    {
        return 'REGION_ID';
    }

    /**
     * @return bool|string
     * @throws ArgumentException
     * @throws SystemException
     */
    public function getTargetEntityName(): string
    {
        return RegionTable::getEntity()->getFullName();
    }
}
