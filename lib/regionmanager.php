<?php

namespace App\Regions;

use Bitrix\Main\Context;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use CCity;
use App\Regions\Detectors\CookieRegionDetector;
use App\Regions\Detectors\DefaultRegionDetector;
use App\Regions\Detectors\DomainRegionDetector;
use App\Regions\Detectors\RegionDetector;
use App\Regions\Location\Finder\CookieLocationFinder;
use App\Regions\Location\Finder\GeoIpLocationFinder;
use App\Regions\Location\Finder\LocationFinder;
use App\Regions\Location\Finder\RegionBasedLocationFinder;
use App\Regions\Location\Finder\RequestLocationFinder;
use App\Regions\Location\Finder\StatisticLocationFinder;
use App\Regions\Location\Location;
use App\Regions\Location\Repository\LocationRepository;
use App\Regions\Location\Repository\SaleLocationRepository;
use App\Regions\Tables\RegionLocationTable;
use App\Regions\Tables\RegionTable;

class RegionManager
{
    private static $instance;

    /**
     * @var RegionDetector[]
     */
    private $regionDetectors = [];

    /**
     * @var LocationFinder[]
     */
    private $locationFinders = [];

    /**
     * @var array
     */
    private $currentRegion;

    /**
     * @var LocationRepository
     */
    private $locationRepository;

    /**
     * @var array
     */
    private $currentLocation;

    /**
     * Singleton
     *
     * @throws LoaderException
     */
    private function __construct()
    {
        $this->init();
    }

    /**
     * @return RegionManager
     * @throws LoaderException
     */
    public static function getInstance(): RegionManager
    {
        if (!static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * @throws LoaderException
     */
    public function init(): void
    {
        Loader::requireModule('sale');

        $this->locationRepository = new SaleLocationRepository();

        if (Helpers::isMultiDomainEnabled()) {
            $this->addRegionDetector(new DomainRegionDetector());
        } else {
            $this->addRegionDetector(new CookieRegionDetector());
        }
        $this->addRegionDetector(new DefaultRegionDetector());

        $this->addLocationFinder(new CookieLocationFinder($this->locationRepository));
        $this->addLocationFinder(new RequestLocationFinder($this->locationRepository));
        $this->addLocationFinder(new GeoIpLocationFinder($this->locationRepository));
        if (Loader::includeModule('statistic')) {
            $this->addLocationFinder(new StatisticLocationFinder(new CCity(), $this->locationRepository));
        }
        $this->addLocationFinder(new RegionBasedLocationFinder($this, $this->locationRepository));
    }

    public function addRegionDetector(RegionDetector $detector): void
    {
        $this->regionDetectors[get_class($detector)] = $detector;
    }

    public function addLocationFinder(LocationFinder $finder): void
    {
        $this->locationFinders[get_class($finder)] = $finder;
    }

    public function getRegionByLocationId(int $locationId): ?array
    {
        $result = RegionLocationTable::getConnectedEntites($locationId, [
            'select' => [
                'ID',
                'SORT',
            ],
        ])->fetchAll();

        // по-возрастанию
        usort($result, function ($a, $b) {
            return $a['SORT'] <=> $b['SORT'];
        });

        $result = reset($result);

        if (!$result) {
            $result['ID'] = Helpers::getDefaultRegionId();
        }

        return RegionTable::getRowById($result['ID']);
    }

    public function getCurrentLocation(): ?Location
    {
        if (!$this->currentLocation) {
            foreach ($this->locationFinders as $finder) {
                if ($this->currentLocation = $finder->find()) {
                    break;
                }
            }
        }

        return $this->currentLocation;
    }

    /**
     * @return LocationRepository
     */
    public function getLocationRepository(): LocationRepository
    {
        return $this->locationRepository;
    }

    public function getRedirectUrl($region = null, $path = ''): string
    {
        if (!$region) {
            $region = $this->getCurrentRegion();
        }

        if (!$region) {
            return '';
        }

        $currentDomain = Helpers::getDomainInfo();
        $regionDomain = '';

        if (Helpers::isMultiDomainEnabled() && (int)$region['ID'] !== Helpers::getDefaultRegionId()) {
            $regionDomain = $region['SUB_DOMAIN'];
        }

        if ($currentDomain['SUB_DOMAIN'] !== $regionDomain) {
            $context = Context::getCurrent();
            $domain = $currentDomain['DOMAIN'];
            if ($regionDomain) {
                $domain = $regionDomain . '.' . $domain;
            }
            $protocol = $context->getRequest()->isHttps() ? 'https' : 'http';
            return sprintf('%s://%s%s', $protocol, $domain, $path);
        }

        return '';
    }

    public function getCurrentRegion(): ?array
    {
        if (!$this->currentRegion) {
            foreach ($this->regionDetectors as $detector) {
                if ($regionId = $detector->getRegionId()) {
                    if ($this->currentRegion = RegionTable::getRow([
                        'filter' => ['ID' => $regionId],
                        'select' => ['*', 'UF_*']
                    ])) {
                        break;
                    }
                }
            }
        }

        return $this->currentRegion;
    }

    /**
     * Изменяет цену в зависимости от региональных предустонавок
     *
     * @param $price
     *
     * @return mixed
     */
    public function modifyPrice(float $price): float
    {
        if ($region = $this->getCurrentRegion()) {
            $priceDifference = max(floatval($region['PRICE_VALUE']), 0);
            switch ($region['PRICE_MOD']) {
                case RegionTable::FIX_UP:
                    return $price + $priceDifference;
                case RegionTable::FIX_DOWN:
                    $finalPrice = $price - $priceDifference;

                    return $finalPrice > 0 ? $finalPrice : $price;
                case RegionTable::PERCENT_UP:
                    $priceDifference = ($price / 100.00) * $priceDifference;

                    return $price + $priceDifference;
                case RegionTable::PERCENT_DOWN:
                    $priceDifference = ($price / 100.00) * $priceDifference;
                    $finalPrice = $price - $priceDifference;

                    return $finalPrice > 0 ? $finalPrice : $price;
                default:
                    break;
            }
        }

        return $price;
    }
}
