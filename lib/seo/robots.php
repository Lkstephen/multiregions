<?php

namespace App\Regions\Seo;

use RobotsTxtParser\RobotsTxtParser;

/**
 * Класс для преобразования файла robots.txt в мультидоменный формат
 *
 * @package App\Regions\Seo
 */
class Robots
{
    /**
     * @var RobotsTxtParser
     */
    private $parser;

    /**
     * Robots constructor.
     * @param  string  $content  - содержимое robots.txt
     */
    public function __construct(string $content)
    {
        $this->parser = new RobotsTxtParser($content);
    }

    public function getContent($eol = "\r\n")
    {
        $ruleSet = $this->parser->getRules();
        $ruleSet = $this->modifyRules($ruleSet);
        krsort($ruleSet);
        $output = [];
        foreach ($ruleSet as $userAgent => $rules) {
            $output[] = 'User-agent: ' . $userAgent;
            foreach ($rules as $directive => $value) {
                // Not multibyte
                $directive = ucfirst($directive);
                if (is_array($value)) {
                    // Shorter paths later
                    usort($value, function ($a, $b) {
                        return mb_strlen($a) < mb_strlen($b);
                    });
                    foreach ($value as $subValue) {
                        $output[] = $directive . ': ' . $subValue;
                    }
                } else {
                    $output[] = $directive . ': ' . $value;
                }
            }
            $output[] = '';
        }
        $output[] = '';

        return implode($eol, $output);
    }

    private function modifyRules($ruleSet)
    {
        $replacement = $_SERVER['HTTP_HOST'];
        foreach ($ruleSet as $userAgent => $rules) {

            // Замена Host
            $subject = $rules[RobotsTxtParser::DIRECTIVE_HOST];
            $pattern = parse_url($subject, PHP_URL_HOST);
            if ($pattern) {
                $ruleSet[$userAgent][RobotsTxtParser::DIRECTIVE_HOST]
                    = preg_replace("/$pattern/", $replacement, $subject);
            }

            // Замена Sitemap
            $subjects = $rules[RobotsTxtParser::DIRECTIVE_SITEMAP];
            foreach ($subjects as $subject) {
                $pattern = parse_url($subject, PHP_URL_HOST);
                if ($pattern) {
                    $ruleSet[$userAgent][RobotsTxtParser::DIRECTIVE_SITEMAP]
                        = preg_replace("/$pattern/", $replacement, $subject);
                }
            }
        }

        return $ruleSet;
    }
}
