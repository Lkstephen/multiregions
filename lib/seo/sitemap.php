<?php

namespace App\Regions\Seo;

use App\Regions\Helpers;

/**
 * Класс для преобразования файла sitemap.xml в мультидоменный вид
 *
 * @package App\Regions\Seo
 */
class Sitemap
{
    private $domainName;

    public function __construct($domainName)
    {
        $this->domainName = $domainName;
    }

    /**
     * Парсит полученный файл sitemap.xml в массив. <br>
     * Если sitemap.xml содержит ссылки на другие файлы sitemap.xml они так же будут обработаны.
     *
     * @param $path
     * @return array|bool
     */
    public function parse($path)
    {
        if (!Helpers::isMultiDomainEnabled()) {
            return false;
        }

        if (!file_exists($path)) {
            return false;
        }

        $rootNode = simplexml_load_file($path);

        if (!$rootNode) {
            return false;
        }

        $siteMap = [];

        foreach ($rootNode as $directive => $element) {
            switch ($directive) {
                case 'sitemap':
                    $rootDir = dirname($path);
                    $filePath = $this->getLocalSiteMap($rootDir, $element->loc);
                    if ($childSiteMap = $this->parse($filePath)) {
                        $siteMap = array_merge($siteMap, $childSiteMap);
                    }
                    break;
                case 'url':
                    $siteMap[] = [
                        'loc' => $this->replaceDomainName($element->loc),
                        'lastmod' => (string)$element->lastmod,
                    ];

                    break;
                default:
                    break;
            }
        }

        return $siteMap;
    }

    /**
     * Пытается получить карту сайта из файловой системы, вместо URL.
     *
     * @param $rootDir  - Директория для поиска карты сайта
     * @param $url  - URL карты сайта
     * @return string - Путь к карте сайта
     */
    private function getLocalSiteMap($rootDir, $url): string
    {
        if ($path = parse_url($url, PHP_URL_PATH)) {
            $filePath = $rootDir . $path;
            if (file_exists($filePath)) {
                return $filePath;
            }
        }

        return $url;
    }

    /**
     * Заменяет доменное имя в ссылке.
     *
     * @param $url
     * @return string
     */
    private function replaceDomainName($url): string
    {
        $pattern = parse_url($url, PHP_URL_HOST);

        return preg_replace("/$pattern/", $this->domainName, $url);
    }

    /**
     * Преобразует массив в sitemap.xml файл.
     *
     * @param  array  $siteMapArray
     * @return string
     */
    public function xmlFromArray(array $siteMapArray): string
    {
        $xml[] = '<?xml version="1.0" encoding="UTF-8"?>';
        $xml[] = '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        foreach ($siteMapArray as $item) {
            $xml[] = '<url>';
            $xml[] = "<loc>{$item['loc']}</loc>";
            $xml[] = "<lastmod>{$item['lastmod']}</lastmod>";
            $xml[] = '</url>';
        }
        $xml[] = '</urlset>';

        return implode('', $xml);
    }
}
