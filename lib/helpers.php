<?php

namespace App\Regions;

use Bitrix\Main\Config\Option;
use Bitrix\Main\Context;
use CSite;
use AppRegions;

final class Helpers
{
    private function __construct()
    {
    }

    /**
     * Возвращает список доменных имён, привязанных к текущему сайту битрикс
     *
     * @return array
     */
    public static function getDomainInfo(): array
    {
        $siteId = CSite::GetDefSite(SITE_ID);
        $site = CSite::GetByID($siteId)->Fetch();
        $domains = array_unique(array_merge(
            [$site['SERVER_NAME']],
            preg_split("/\r\n|\n|\r/", $site['DOMAINS'], -1, PREG_SPLIT_NO_EMPTY)
        ));
        $server = Context::getCurrent()->getServer();
        $hostName = $server->getHttpHost();
        $hostName = explode(':', $hostName)[0]; // В том случае когда в конце указан порт
        foreach ($domains as $domain) {
            if (mb_strpos($hostName, $domain) !== false) {
                return [
                    'DOMAIN' => $domain,
                    'SUB_DOMAIN' => mb_strtolower(rtrim(mb_substr($hostName, 0, mb_strlen($hostName) - mb_strlen($domain)), '.'))
                ];
            }
        }

        return ['DOMAIN' => reset($domains)];
    }

    public static function isMultiDomainEnabled(): bool
    {
        return Option::get(AppRegions::MODULE_ID, 'multi_domain', 'N') === 'Y';
    }

    public static function getDefaultRegionId(): int
    {
        return (int)Option::get(AppRegions::MODULE_ID, 'default_region', 1);
    }
}
