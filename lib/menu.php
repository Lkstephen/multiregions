<?php

namespace App\Regions;

use AppRegions;

class Menu
{
    public static function getAdminMenu(
        &$arGlobalMenu,
        &$arModuleMenu
    ) {
        if (!isset($arGlobalMenu['global_menu_app'])) {
            $arGlobalMenu['global_menu_app'] = [
                'menu_id' => 'app',
                'text' => 'Awteam',
                'title' => 'Awteam',
                'sort' => 500,
                'items_id' => 'global_menu_app_items',
                "icon" => "",
                "page_icon" => "",
            ];
        }

        $moduleId = AppRegions::MODULE_ID;
        global $APPLICATION;
        if ($APPLICATION->GetGroupRight($moduleId) != "D") {
            $aMenu = [
                'parent_menu' => 'global_menu_app',
                'section' => 'app.regions',
                'sort' => 350,
                'text' => 'Мультирегиональность',
                'title' => 'Мультирегиональность',
                'icon' => 'app_regions_menu_icon',
                'page_icon' => 'app_page_icon',
                'items_id' => 'menu_app.regions',
                'dynamic' => true,
                'items' => [
/*                    [
                        'text' => 'Настройки',
                        'url' => 'app_regions_settings.php' . '?lang=' . LANGUAGE_ID,
                        'title' => 'Заголовок 2',
                        'items_id' => 'menu_app.regions_settings',
                        'more_url' => [
                            '#',
                        ],
                    ],*/
                    [
                        'text' => 'Список регионов',
                        'url' => 'app_regions_list.php' . '?lang=' . LANGUAGE_ID,
                        'title' => 'Заголовок 1',
                        'items_id' => 'menu_app.regions_list',
                        'more_url' => [
                            '#',
                        ],
                    ],
                ],
            ];

            $arGlobalMenu['global_menu_app']['items']['app.regions'] = $aMenu;
        }
    }
}

