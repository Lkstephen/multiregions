<?php

namespace App\Regions\Location;

use Bitrix\Main\Type\Contract\Arrayable;
use JsonSerializable;

/**
 * Реализация простого типа местоположения
 *
 * @package App\Regions
 */
class SimpleLocation implements Location, JsonSerializable, Arrayable
{
    /**
     * @var int|null
     */
    private $id;

    /**
     * @var int|null
     */
    private $type;

    /**
     * @var string|null
     */
    private $name;

    /**
     * Specify data which should be serialized to JSON
     *
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'type' => $this->type,
        ];
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return SimpleLocation
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param  int  $type
     *
     * @return SimpleLocation
     */
    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return SimpleLocation
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
