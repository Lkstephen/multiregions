<?php

namespace App\Regions\Location\Finder;

use App\Regions\Location\Location;

/**
 * Класс должен определять местоположение пользователя
 *
 * @package App\Regions\Location\Finder
 */
interface LocationFinder
{
    function find(): ?Location;
}
