<?php

namespace App\Regions\Location\Finder;

use CCity;
use InvalidArgumentException;
use App\Regions\Location\Location;
use App\Regions\Location\Repository\LocationRepository;

/**
 * Определение местоположения пользователя по IP на основе данных модуля statistics в битрикс.
 *
 * @package App\Regions\Location\Finder
 */
final class StatisticLocationFinder implements LocationFinder
{
    /**
     * @var CCity
     */
    private $provider;

    /**
     * @var LocationRepository
     */
    private $locationRepository;

    /**
     * StatisticLocationDetector constructor.
     *
     * @param CCity $provider
     * @param LocationRepository $locationRepository
     */
    public function __construct(CCity $provider, LocationRepository $locationRepository)
    {
        if (!$provider) {
            throw new InvalidArgumentException('provider');
        }
        $this->provider = $provider;

        if (!$locationRepository) {
            throw new InvalidArgumentException('locationService');
        }
        $this->locationRepository = $locationRepository;
    }

    public function find(): ?Location
    {
        if ($info = $this->provider->GetFullInfo()) {
            return $this->locationRepository->getLocationByCityName($info['CITY_NAME']['VALUE']);
        }

        return null;
    }
}
