<?php

namespace App\Regions\Location\Finder;

use Bitrix\Main\Context;
use InvalidArgumentException;
use App\Regions\Location\Location;
use App\Regions\Location\Repository\LocationRepository;

/**
 * Определение местоположения по параметрам в HTTP запросе.
 *
 * @package App\Regions\Location\Finder
 */
final class RequestLocationFinder implements LocationFinder
{
    const LOCATION_KEY = 'cityId';

    /**
     * @var LocationRepository
     */
    private $locationRepository;

    /**
     * CookieLocationDetector constructor.
     *
     * @param LocationRepository $locationRepository
     */
    public function __construct(LocationRepository $locationRepository)
    {
        if (!$locationRepository) {
            throw new InvalidArgumentException('locationService');
        }
        $this->locationRepository = $locationRepository;
    }

    public function find(): ?Location
    {
        $request = Context::getCurrent()->getRequest();
        $locationId = (int)$request->get(self::LOCATION_KEY);

        return $this->locationRepository->getLocationById($locationId);
    }
}
