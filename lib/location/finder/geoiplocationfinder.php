<?php

namespace App\Regions\Location\Finder;

use Bitrix\Main\Service\GeoIp\Manager;
use InvalidArgumentException;
use App\Regions\Location\Location;
use App\Regions\Location\Repository\LocationRepository;

/**
 * Определяет местоположение пользователя по IP на основе данных сервиса GeoIP в битрикс.
 *
 * @see https://dev.1c-bitrix.ru/api_d7/bitrix/main/service/geoip/index.php
 * @package App\Regions\Location\Finder
 */
final class GeoIpLocationFinder implements LocationFinder
{
    /**
     * @var LocationRepository
     */
    private $locationRepository;

    /**
     * GeoIpRegionDetector constructor.
     *
     * @param  LocationRepository  $locationRepository
     */
    public function __construct(LocationRepository $locationRepository)
    {
        if (!$locationRepository) {
            throw new InvalidArgumentException('locationRepository');
        }

        $this->locationRepository = $locationRepository;
    }

    public function find(): ?Location
    {
        if ($cityName = $this->getCityName()) {
            return $this->locationRepository->getLocationByCityName($cityName);
        }

        return null;
    }

    private function getCityName(): ?string
    {
        $ip = Manager::getRealIp();
        $result = Manager::getDataResult($ip, LANGUAGE_ID, ['countryName', 'cityName']);
        if ($result->isSuccess()) {
            return $result->getGeoData()->cityName;
        }

        return null;
    }
}
