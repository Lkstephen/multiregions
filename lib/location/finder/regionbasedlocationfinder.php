<?php

namespace App\Regions\Location\Finder;

use InvalidArgumentException;
use App\Regions\Location\Location;
use App\Regions\Location\Repository\LocationRepository;
use App\Regions\RegionManager;

/**
 * Определяет местоположение пользователя по региону открытого им сайта. <br>
 * Полезно для тех случаев, когда невозможно определить регион пользователя иным способом.
 *
 * @package App\Regions\Location\Finder
 */
final class RegionBasedLocationFinder implements LocationFinder
{
    /**
     * @var RegionManager
     */
    private $regionManager;

    /**
     * @var LocationRepository
     */
    private $locationRepository;

    public function __construct(RegionManager $regionManager, LocationRepository $locationRepository)
    {
        if (!$regionManager) {
            throw new InvalidArgumentException('regionManager');
        }
        if (!$locationRepository) {
            throw new InvalidArgumentException('locationRepository');
        }

        $this->regionManager = $regionManager;
        $this->locationRepository = $locationRepository;
    }

    function find(): ?Location
    {
        if ($region = $this->regionManager->getCurrentRegion()) {
            return $this->locationRepository->getLocationByCode($region['LOCATION_CODE']);
        }

        return null;
    }
}
