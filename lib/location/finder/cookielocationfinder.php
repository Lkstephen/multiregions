<?php

namespace App\Regions\Location\Finder;

use Bitrix\Main\Context;
use InvalidArgumentException;
use App\Regions\Location\Location;
use App\Regions\Location\Repository\LocationRepository;

/**
 * Определение местоположения пользователя по Cookie. <br>
 * В том случае, когда пользователь ранее заходил на сайт и задал своё местоположение.
 *
 * @package App\Regions\Location\Finder
 */
final class CookieLocationFinder implements LocationFinder
{
    const LOCATION_COOKIE = 'app_regions_location_id';

    /**
     * @var LocationRepository
     */
    private $locationRepository;

    /**
     * CookieLocationDetector constructor.
     *
     * @param LocationRepository $locationRepository
     */
    public function __construct(LocationRepository $locationRepository)
    {
        if (!$locationRepository) {
            throw new InvalidArgumentException('locationService');
        }
        $this->locationRepository = $locationRepository;
    }

    public function find(): ?Location
    {
        $request = Context::getCurrent()->getRequest();
        $locationId = (int)$request->getCookieRaw(self::LOCATION_COOKIE);

        return $this->locationRepository->getLocationById($locationId);
    }
}
