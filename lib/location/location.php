<?php

namespace App\Regions\Location;

/**
 * Должен содержать информацию по местоположению
 *
 * @package App\Regions\Location
 */
interface Location
{
    /**
     * Уникальный идентификатор местоположения
     *
     * @return int
     */
    public function getId(): int;

    /**
     * Идентификатор типа местоположения. <br>
     * Типом является: Страна, Город, Область и т.д.
     *
     * @return int
     */
    public function getType(): int;

    /**
     * Наименование местоположения
     *
     * @return string
     */
    public function getName(): string;
}
