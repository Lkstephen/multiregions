<?php

namespace App\Regions\Location\Repository;

use Bitrix\Main\ArgumentException;
use Bitrix\Main\ObjectPropertyException;
use Bitrix\Main\SystemException;
use Bitrix\Sale\Location\DefaultSiteTable;
use Bitrix\Sale\Location\LocationTable;
use App\Regions\Location\Location;
use App\Regions\Location\SimpleLocation;

/**
 * Получает информацию по местоположениям на основе данных модуля Sale в Битрикс
 *
 * @package App\Regions\Location\Repository
 */
final class SaleLocationRepository implements LocationRepository
{
    /**
     * Тип местоположения битрикс
     */
    const COUNTRY_TYPE = 1;

    /**
     * Тип местоположения битрикс
     */
    const CITY_TYPE = 5;

    /**
     * @return array
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    public function getCountries(): array
    {
        return $this->getLocationsByType(self::COUNTRY_TYPE);
    }

    /**
     * @param int $typeId
     *
     * @return array
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    private function getLocationsByType(int $typeId): array
    {
        if (!$typeId) {
            return [];
        }

        $query = LocationTable::getList([
            'filter' => [
                '=TYPE_ID' => $typeId,
                '=NAME.LANGUAGE_ID' => LANGUAGE_ID,
            ],
            'order' => [
                'SORT' => 'asc',
                'TYPE_ID' => 'asc',
                'NAME.NAME' => 'asc',
            ],
            'select' => [
                'ID',
                'TYPE_ID',
                'LOCATION_NAME' => 'NAME.NAME',
            ],
            'cache' => [
                'ttl' => 36000000,
            ],
        ]);

        $locations = [];

        foreach ($query->fetchAll() as $item) {
            $locations[] = (new SimpleLocation())->setId($item['ID'])->setType($item['TYPE_ID'])->setName($item['LOCATION_NAME']);
        }

        return $locations;
    }

    /**
     * @return array
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    public function getCities(): array
    {
        return $this->getLocationsByType(self::CITY_TYPE);
    }

    /**
     * @return array
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    public function getFavorites(): array
    {
        $query = DefaultSiteTable::getList([
            'select' => [
                'ID' => 'LOCATION.ID',
                'TYPE_ID' => 'LOCATION.TYPE_ID',
                'LOCATION_NAME' => 'LOCATION.NAME.NAME',
            ],
            'order' => [
                'SORT' => 'asc',
                'LOCATION.NAME.NAME' => 'asc',
            ],
            'filter' => [
                '=SITE_ID' => SITE_ID,
                '=LOCATION.NAME.LANGUAGE_ID' => LANGUAGE_ID,
            ],
        ]);

        $favorites = [];

        foreach ($query->fetchAll() as $item) {
            $favorites[] = (new SimpleLocation())->setId($item['ID'])->setType($item['TYPE_ID'])->setName($item['LOCATION_NAME']);;
        }

        return $favorites;
    }

    /**
     * @param string $name
     *
     * @return Location|null
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    public function getLocationByCityName(string $name): ?Location
    {
        if (!$name) {
            return null;
        }

        $row = LocationTable::getList([
            'filter' => [
                '=NAME.NAME' => $name,
                '=NAME.LANGUAGE_ID' => LANGUAGE_ID,
            ],
            'select' => [
                'ID',
                'TYPE_ID',
                'LOCATION_NAME' => 'NAME.NAME',
            ],
            'cache' => [
                'ttl' => 36000000,
            ],
        ])->fetch();

        if ($row) {
            $location = (new SimpleLocation())->setId($row['ID'])->setType($row['TYPE_ID'])->setName($row['LOCATION_NAME']);

            return $location;
        }

        return null;
    }

    /**
     * @param int $id
     *
     * @return Location|null
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    public function getLocationById(int $id): ?Location
    {
        if (!$id) {
            return null;
        }

        $row = LocationTable::getList([
            'filter' => [
                '=ID' => $id,
                '=NAME.LANGUAGE_ID' => LANGUAGE_ID,
            ],
            'select' => [
                'ID',
                'TYPE_ID',
                'LOCATION_NAME' => 'NAME.NAME',
            ],
            'cache' => [
                'ttl' => 36000000,
            ],
        ])->fetch();

        if ($row) {
            $location = (new SimpleLocation())->setId($row['ID'])->setType($row['TYPE_ID'])->setName($row['LOCATION_NAME']);

            return $location;
        }

        return null;
    }

    /**
     * @param string $code
     *
     * @return Location|null
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    public function getLocationByCode(string $code): ?Location
    {
        if (!$code) {
            return null;
        }

        $row = LocationTable::getRow([
            'filter' => [
                '=CODE' => $code,
                '=NAME.LANGUAGE_ID' => LANGUAGE_ID,
            ],
            'select' => [
                'ID',
                'TYPE_ID',
                'LOCATION_NAME' => 'NAME.NAME',
            ],
            'cache' => [
                'ttl' => 36000000,
            ],
        ]);
        $location = null;
        if ($row) {
            $location = (new SimpleLocation())->setId($row['ID'])->setType($row['TYPE_ID'])->setName($row['LOCATION_NAME']);
        }

        return $location;
    }
}
