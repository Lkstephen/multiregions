<?php

namespace App\Regions\Location\Repository;

use App\Regions\Location\Location;

interface LocationRepository
{
    /**
     * Возвращает список стран.
     *
     * @return array
     */
    public function getCountries(): array;

    /**
     * Возвращает города, доступные пользователю для выбора.
     *
     * @return array
     */
    public function getCities(): array;

    /**
     * Возвращает список избранных местоположений для быстрого выбора.
     *
     * @return array
     */
    public function getFavorites(): array;

    /**
     * Получает местоположение по ID.
     *
     * @param  int  $id
     * @return Location|null
     */
    public function getLocationById(int $id): ?Location;

    /**
     * Получает местоположение по коду.
     *
     * @param  string  $code
     * @return Location|null
     */
    public function getLocationByCode(string $code): ?Location;

    /**
     * Возвращает местоположение по имени города.
     *
     * @param  string  $name
     * @return Location|null
     */
    public function getLocationByCityName(string $name): ?Location;
}
